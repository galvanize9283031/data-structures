# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.

class LinkedQueueNode:
    def __init__(self, value, next=None) -> None:
        self.value = value
        self.link = next

class LinkedQueue:
    pass
    def __init__(self) -> None:
        self.head = None
        self.tail = None
        self.size = 0

    def dequeue(self):
        if self.size == 0:
            raise Exception("Empty queue")
        else:
            value = self.head.value
            self.head = self.head.link
            self.size -= 1
            if self.size == 0:
                self.tail = None
            return value

    def enqueue(self, value):
        if self.size == 0:
            self.head = LinkedQueueNode(value)
            self.tail = self.head
            self.size += 1
        else:
            self.tail.link = LinkedQueueNode(value)
            self.tail = self.tail.link
            self.size += 1
