# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedListNode:
    def __init__(self, value, next=None) -> None:
        self.value = value
        self.link = next


class LinkedList:
    def __init__(self) -> None:
        self.head = None
        self.tail = None
        self.length = 0

    def insert(self, value, position=None):
        if self.head is None:
            self.head = LinkedListNode(value)
            self.tail = self.head
        else:
            if position == None and self.length != 0:
                self.tail.link = LinkedListNode(value)
                self.tail = self.tail.link
            elif position == 0:
                self.head = LinkedListNode(value, self.head)
            else:
                current_node = self.head
                current_position = 0
                while current_position < position - 1:
                    current_node = current_node.link
                    current_position += 1
                current_node.link = LinkedListNode(value, current_node.link)
        self.length += 1

    def get(self, position):
        if position < 0:
            position = self.length + position
        if position >= self.length or position < 0:
            raise IndexError("Index out of range")
        current_node = self.head
        current_position = 0
        while current_position < position:
            current_node = current_node.link
            current_position += 1
        return current_node.value

    def remove(self, position):
        if position < 0:
            position = self.length + position
        if position >= self.length or position < 0:
            raise IndexError("Index out of range")
        out = None
        if position == 0:
            out = self.head.value
            self.head = self.head.link
        else:
            current_node = self.head
            current_position = 0
            while current_position < position - 1:
                current_node = current_node.link
                current_position += 1
            out = current_node.link.value
            current_node.link = current_node.link.link
            if current_node.link == None:
                self.tail = current_node
        self.length -= 1
        return out
